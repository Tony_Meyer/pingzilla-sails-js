/**
 * MonitorController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    
  
  /**
   * Action blueprints:
   * /monitor/site
   */
   site: function (req, res) {
    var schedule = require('node-schedule');
    
    var rule = new schedule.RecurrenceRule();
    rule.second = [0, 1, 20, 30, 40, 50];
    
    var j = schedule.scheduleJob(rule, function(){
      var request = require('request');
        request('http://www.google.com', function (error, response, body) {
          if (!error && response.statusCode == 200) {
            console.log('woah'+response.statusCode) // Print the google web page.
          }
        })
      });
  },




  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to MonitorController)
   */
  _config: {}

  
};
